
/*
I login and select a room. The room has tasks with snapshots of: 
	1) time required, 
	2) potential cash value, and 
	3) deadlines.
I select three tasks and add them to my today board.
Complete one, then modal requests input to test some independent variables on productivity
Complete all, then app gives avg's and correlation between independents and dependents

====== header bar (motivation_therapy)
======  menu bar
<back  (feedback modal)   next>
login screen, 
rooms screen, 
potential tasks screen, 
active tasks screen, 
task completion modals,
productivity cards screen,
pay me $1, $5, $20 screen,
logout screen
*/
//0-A. edit and submit signup
//0-B. complete profile defining goals as defined below

//1. edit and submit login
//2. select and submit room(s)
//3. create, cp, edit, mv, delete cards
//4. select and submit 'potential tasks'
//5. complete task and update modal, submit all
//6. avg statistics for day, week, month, make linear regression for:
// --> time of day
// --> length of task
// --> closeness of deadline
// --> cash income or other reward
// --> time between tasks (from last task to completed task)
// --> expenses
// --> liked it or hated it
//7. donate $1, $5, $20
//8. logout

/*
Motivational Therapy is an app devoted to 
boosting your productivity. The technology herein breaks up
tasks into small manageable chunks and tracks 
completion analytics on each task you finish.
As the app learns your habits, tasks are automatically re-organized 
to increase likelihood of success and align with your goals.
*/

//-->this.renderSubmit()
// on page 1 and page 7 show nothing (single action)
// on page 2-6 show next unless submit object is ready, then show submit button

//In the machine learning step, we can generate an ideal day for a person. This is like the mario problem: we input some average days and then we output a similar day based on the average days. The real interesting part will be after outputting and randomly generated day, testing it's viability against our statistics. Maybe we output three days and see what is likely to happen statistics-wise based on each day, and then pick the day that best suits us.

//generate a three independent variable, one dependent variable linear regression curve for the day based on statistics taken on active tasks
//-->Independent variables: length of task, length of time between tasks, time of day
//-->Dependent variable: happiness level

//generate some sort of linear regression curve
//generate some sort of normal distribution curve

//generate some new days based on a couple input days
//test the statistics of the new days based on the linear regression curve and the normal distribution curves


//so my customer is chris mcmillan and stephanie wong mcmillan - they need to present me with a problem, and I need to give them a solution in two weeks. 

//* I could say - hey, give me something you need done, and I will get it done in two weeks.\

//* My customer could be eugene - he has a problem - he wants me to file a reduced value claim against the car insurance of the person that hit him. I could say - hey, I will get this done in two weeks.

// I could be the customer - I have a problem - Eugene asked me to do something, and I put it on the back burner, and now I don't ever really feel like doing it. My problem would be fixed by re-engaging the customer, Eugene, and showing him progress.

//Unfortunately, none of these things involve the super awesome AI, that I have currently been thinking about. So let's re-think. I have an idea for taking tasks including R&R and maybe performing some sort of sentiment analysis to make sure that the user (me) is not getting over worked and/or working on things that cause him to slowly die inside. The output could be re-organizing tasks, suggesting tasks, or even suggesting goals.

//Ok, so let's redefine the use-case. We have me, and I have two problems: 1) I should push forward or push back on helping Eugene with his Diminished Value Claim, and 2) I should push forward or push back on helping Ben with his book. I could...

//1) define a small chunk of the project, 2) work on it, 3) write a brief paragraph about how I feel about the work I did, and then have 4) the software suggest ways to increase positive sentiment regarding that work. Suggestions should include (provide small chunk to customer for feedback, shorten or lengthen length of time between this subtask and the next subtask in the set, and/or define a better time in the week/day to work on the task)

//March 1, 2008
//1)-->login by looking up a users_table in the database
//2)-->connect the users table to a rooms_table in the database
//3)-->store policy, task, and gantt objects in rooms_table
//4)-->get paypal working
//5)-->logout by dissassociating room_obj with client_id