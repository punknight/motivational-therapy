var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var serveStatic = require('serve-static');
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var session = require('express-session');

var CurrentSessions = {};
//[session.id]={socket_id: socket.id, user_id: user.id}
var CurrentSockets = {};
//[socket.id] = {user_obj: user_obj, room_obj: room_obj, room_ids: []}
var CurrentRooms = {};
//[room.id] = {roomname: room_obj.name, users[user.id]: {email: user.email}}

app.use(session({
  secret: 'dogs are better',
  resave: false,
  saveUninitialized: true
}));

mongoose.connect('mongodb://localhost/test')
	.then(function(){
		console.log('connected to db');
	})
	.catch(function(){
		console.log('not connected to db, make sure mongo is on');
	});

var UserSchema = new mongoose.Schema({ email: String, password: String});

UserSchema.statics.bootstrapDB = function(cb){
	function createHashPassword(password, bcrypt, SALT_WORK_FACTOR=10, cb){
		bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
	    bcrypt.hash(password, salt, function(err, hash) {
	  		return cb(false, hash)
	  	});
	  });
	}      

	User.findOne({}, function(err, user_obj) {
	  if (err) throw err;
	  if (user_obj===null){
	  	createHashPassword('pass1', bcrypt, 10, function(err, hash){
	  		if(err) throw err;
	  		var U1 = new User({email: 'user1@mail.com', password: hash, calendar: []});
	  		var U2 = new User({email: 'user2@mail.com', password: hash, calendar: []});
	  		var U3 = new User({email: 'user3@mail.com', password: hash, calendar: []});
	  		U1.save();
	  		U2.save();
	  		U3.save();
	  		return cb(false, 'Users bootstrapped', [U1, U2, U3]);
	  	});
	  } else {
			User.remove({}, function(){
				cb(true, 'Users removed, continuing on to remove rooms');
			});
	  }
	});
}
UserSchema.statics.getByUserID = function(user_id, cb){
	User.findOne({_id: user_id}, function(err, user_obj){
		if(user_obj===null || err){
			return cb(true, 'no user found'+err);
		} else {
			return cb(false, 'user found', user_obj);
		}
	});
}

var RoomSchema = new mongoose.Schema({ name: String, policy_arr: [], task_arr: [], gantt_arr: [] });
RoomSchema.statics.bootstrapDB = function(cb){
	Room.findOne({}, function(err, room_obj){
		if(err) throw err;
		if(room_obj===null){
			var policy_id1 = generateID('policy');
			var policy_id2 = generateID('policy');
			var policy_arr = [
				{id: policy_id1, name: 'Wag', description: 'Make $1000 using the Wag app by the end of the month.', numerator_hours: 0.5, denominator_hours: 2},
				{id: policy_id2, name: 'TaskRabbit', description: 'Make $2000 using the TaskRabbit app by the end of the month.', numerator_hours: 2, denominator_hours: 4}
			];
			var gantt_arr = [];
			gantt_arr.push({goal_id: 'goal-A', calendar_id: '2018-3-11', week_id: '2018-wk15', quarter_id: '2018-q2', numerator_hours: 2, denominator_hours: 4});
			gantt_arr.push({goal_id: 'goal-A', calendar_id: '2018-3-13', week_id: '2018-wk15', quarter_id: '2018-q2', numerator_hours: 2, denominator_hours: 4});
			gantt_arr.push({goal_id: 'goal-B', calendar_id: '2018-3-12', week_id: '2018-wk15', quarter_id: '2018-q2', numerator_hours: 3, denominator_hours: 4});
			gantt_arr.push({goal_id: 'goal-B', calendar_id: '2018-3-0', week_id: '2018-wk15', quarter_id: '2018-q2', numerator_hours: 2, denominator_hours: 3});
			var R1 = new Room({name: 'r1', policy_arr: policy_arr, task_arr: [], gantt_arr: gantt_arr});
			var R2 = new Room({name: 'r2', policy_arr: [], task_arr: [], gantt_arr: []});
			R1.save();
			R2.save();
			cb(false, 'Rooms bootstrapped', [R1, R2]);
		} else {
			Room.remove({}, function(){
				return cb(true, 'Rooms removed, continuing on to remove UserRooms');
			});
		}
	});
}
RoomSchema.statics.getByUserRooms = function(UserRoom_arr, cb){
	
	var RoomIds = UserRoom_arr.map((UserRoom_obj)=>{
		return (new mongoose.Types.ObjectId(UserRoom_obj.room_id));
	});
	
	Room.find()
  .where('_id')
  .in(RoomIds)
  .exec(function (err, Rooms_arr) {
    //make magic happen
		if(Rooms_arr === null || Rooms_arr.length === 0){
			cb(true, 'no room objects found');
		} else {
			var msg = 'rooms found: '+ JSON.stringify(Rooms_arr);
			cb(false, msg, Rooms_arr);
		}
	});
}
RoomSchema.statics.getByRoomID = function(room_id, cb){
	
	Room.findOne({_id: room_id}, function(err, room_obj){
		if(room_obj===null || err){

			return cb(true, 'no room found'+err);
		} else {
			
			return cb(false, 'room found', room_obj);
		}
	})
}
RoomSchema.statics.persistData = function(room_id, new_room_obj, cb){
	function updateObj(old_obj, new_obj){
		for (var key in new_obj){
			old_obj[key] = new_obj[key];
		}
	}

	Room.findById(room_id, function(err, found_room_obj) {
  	if (err) console.log('err updating');
  	updateObj(found_room_obj, new_room_obj);
  	found_room_obj.save()
   		.catch(function(err){return cb(err, 'failed to update')})
  		.then(function(){return cb(false, 'successfully updated')})
  });
};

var UserRoomSchema = new mongoose.Schema({ user_id: String, room_id: String, auth_arr: [] });
UserRoomSchema.statics.bootstrapDB = function(User_arr, Room_arr, cb){
	if(!User_arr|| !Room_arr) {
		UserRoom.remove({}, function(){
			return cb(true, 'UserRooms removed, reboot server to bootstrap DB');
		});
	} else {
		UserRoom.findOne({}, function(err, user_room_obj){
			if(err) throw err;
			if(user_room_obj===null){
				var UR1 = new UserRoom({ user_id: User_arr[0].id, room_id: Room_arr[0].id, auth_arr: [] });
				var UR2 = new UserRoom({ user_id: User_arr[0].id, room_id: Room_arr[1].id, auth_arr: [] });
				var UR3 = new UserRoom({ user_id: User_arr[1].id, room_id: Room_arr[0].id, auth_arr: [] });
				var UR4 = new UserRoom({ user_id: User_arr[1].id, room_id: Room_arr[1].id, auth_arr: [] });
				var UR5 = new UserRoom({ user_id: User_arr[2].id, room_id: Room_arr[0].id, auth_arr: [] });
				
				UR1.save();
				UR2.save();
				UR3.save();
				UR4.save();
				UR5.save();
				cb(false, 'UserRooms bootstrapped', [UR1, UR2, UR3, UR4, UR5]);
			} else {
				UserRoom.remove({}, function(){
					return cb(true, 'UserRooms removed, reboot server to bootstrap DB');
				});
			}
		});

	}
}
UserRoomSchema.statics.getByUserID = function(user_id, cb){
	
	UserRoom.find({user_id: user_id}, function(err, UserRoom_arr){
		
		if(err) throw err;
		if(UserRoom_arr.length===0 || UserRoom_arr === null){
			return cb(true, 'user does not have access to any rooms'); 
		} else {
			return cb(false, 'successfully pulled UserRooms from DB', UserRoom_arr);
		}
	});
}

var Room = mongoose.model('Room', RoomSchema);
var User = mongoose.model('User', UserSchema);
var UserRoom = mongoose.model('UserRoom', UserRoomSchema);
User.bootstrapDB(function(err, msg, User_arr){
	console.log(msg);
	Room.bootstrapDB(function(err, msg, Room_arr){
		console.log(msg);
		UserRoom.bootstrapDB(User_arr, Room_arr, function(err, msg){
			console.log(msg);
		});
	});
});

app.get('/', function(req, res){
	res.sendFile(__dirname+'/index.html')
});

app.use(serveStatic(__dirname+'/public'));

http.listen(process.env.PORT || 3000, function(){
	console.log('listening on port 3000');
});

io.on('connection', function(socket){
	console.log('a new socket has connected');
	//check the CurrentSessions to see if user is already logged in = isSessionAuth
	//if they are logged in, updateCurrentSockets and UpdateCurrentRooms
	//send the user to the room that they were last in
	var cookies = socket.request.headers.cookie;
	var cookiestring=RegExp("connect.sid"+"[^;]+").exec(cookies);
	var session_id = cookiestring.toString().replace(/^[^=]+./,"");
	var user_id = (CurrentSessions[session_id]) ? CurrentSessions[session_id].user_id : null;
	if(user_id){
		console.log('user ' +user_id+' loaded from previous session');
		//database layer
		User.getByUserID(user_id, function(err, msg, user_obj){
			UserRoom.getByUserID(user_id, function(err, msg, UserRoom_arr){
				Room.getByUserRooms(UserRoom_arr, function(err, msg, Rooms_arr){
					//port the pulled DB data into parameters usable by the business functions
					var room_ids = UserRoom_arr.map((UserRoom)=>{
						return UserRoom.room_id
					});
					//update the business objects
					CurrentSockets[socket.id] = {user_obj: user_obj, room_obj: {}, room_ids: room_ids};
					addNamesToCurrentRooms(CurrentRooms, Rooms_arr, function(err, msg){
						getCurrentRooms(CurrentRooms, room_ids, function(err, msg, found_CurrentRooms){
							io.to(socket.id).emit('ROOMS', found_CurrentRooms);		
							io.to(socket.id).emit('VIEW', 1);
							io.to(socket.id).emit('msg', {err: err, msg: msg});	
						});
					});
				});
			});
		});	
	} 
	socket.on('disconnect', function(){
		//CurrentSession should stay stored,
		//CurrentSockets should be wiped
		//CurrentRooms should be updated to remove the user, any empty rooms should be deleted.

		//remove the user from his/her room
		if(CurrentSockets[socket.id]){
			var room_id = CurrentSockets[socket.id].room_obj.id;
			var user_id = CurrentSockets[socket.id].user_obj.id;
			delete CurrentRooms[room_id].users[user_id];
			Object.keys(CurrentRooms).map((key)=>{
				if(Object.keys(CurrentRooms[key].users).length===0){
					delete CurrentRooms[key];
				}
			});
			//remove the user from the socket
			delete CurrentSockets[user_id];
		}
	});

	socket.on('LOGIN', function(login_obj){
		//CurrentSession should get a user_id
		//CurrentSocket should get a user_obj
		//CurrentRooms should remain the same 
		console.log('LOGIN fired on the server side');
		//db layer
		loginUser(User, {bcrypt: bcrypt}, login_obj, function(err, msg, user_obj){
			UserRoom.getByUserID(user_obj.id, function(err, msg, UserRoom_arr){
				Room.getByUserRooms(UserRoom_arr, function(err, msg, Rooms_arr){
					if(err) {
						io.to(socket.id).emit('msg', {err: err, msg: msg});
					} else {
						
						//port the pulled DB data into parameters usable by the business functions
						var room_ids = UserRoom_arr.map((UserRoom)=>{
							return UserRoom.room_id
						});
						var cookies = socket.request.headers.cookie;
						var cookiestring=RegExp("connect.sid"+"[^;]+").exec(cookies);
						var session_id = cookiestring.toString().replace(/^[^=]+./,"");
						CurrentSessions[session_id] = {user_id: user_obj.id};
						CurrentSockets[socket.id] = {user_obj: user_obj, room_obj: {}, room_ids: room_ids};
						addNamesToCurrentRooms(CurrentRooms, Rooms_arr, function(err, msg){
							getCurrentRooms(CurrentRooms, room_ids, function(err, msg, found_CurrentRooms){
								
								io.to(socket.id).emit('ROOMS', found_CurrentRooms);		
								io.to(socket.id).emit('VIEW', 1);
								io.to(socket.id).emit('msg', {err: err, msg: msg});	
							});
						});
					}
				});
			});
		});
	});
	socket.on('LOGOUT', function(){
		//session needs to be removed
		//socket needs to be removed
		//user needs to be removed from the room, any empty rooms should be deleted
		var room_id = CurrentSockets[socket.id].room_obj.id;
		var user_id = CurrentSockets[socket.id].user_obj.id;
		var cookies = socket.request.headers.cookie;
		var cookiestring=RegExp("connect.sid"+"[^;]+").exec(cookies);
		var session_id = cookiestring.toString().replace(/^[^=]+./,"");
		delete CurrentSessions[session_id];
		if(CurrentRooms[room_id]){
			delete CurrentRooms[room_id].users[user_id];	
		}
		Object.keys(CurrentRooms).map((key)=>{
			if(Object.keys(CurrentRooms[key].users).length===0){
				delete CurrentRooms[key];
			}
		});
		//remove the user from the socket
		delete CurrentSockets[user_id];
		var msg = 'You are logged out.';
		io.to(socket.id).emit('msg', {err: false, msg: msg});	
	});

	socket.on('SELECT_ROOM', function(room_id_obj){
		//session stays the same
		//socket gets updated with a room_obj
		//remove user from old room, and put him in a new room
		var room_id = room_id_obj.room_id;
		var user_id = CurrentSockets[socket.id].user_obj.id;
		//db layer stuff
		User.getByUserID(user_id, function(err, user_obj){
			UserRoom.getByUserID(user_id, function(err, found_UserRooms){
				Room.getByRoomID(room_id, function(err, msg, room_obj){
					//port the pulled DB data into parameters usable by the business functions
					var room_ids = found_UserRooms.map((UserRoom)=>{
						return UserRoom.room_id
					});
					//check auth
					checkAuth(found_UserRooms, user_id, room_id, function(err, msg){
						if(err){
							io.to(socket.id).emit('msg', {err: err, msg: msg});	
						} else {
							//remove the user from the old room if you need to
							if(CurrentSockets[socket.id].room_obj){
								delete CurrentRooms[CurrentSockets[socket.id].room_obj.id].users[user_id];
							}
							// add him to the new room
							CurrentRooms[room_id].users[user_id] = {email: user_obj.email};
							CurrentSockets[socket.id] = {user_obj: user_obj, room_obj: room_obj, room_ids};
							io.to(socket.id).emit('ROOM', room_obj);
							io.to(socket.id).emit('VIEW', 2);
							io.to(socket.id).emit('msg', {err: err, msg: msg});	
						}
					});
				});
			});
		});
	});
	socket.on('POTENTIAL_TASKS', function(args_obj){
		console.log('args: \''+JSON.stringify(args_obj)+'\' received');
		var policy_ids = args_obj.selected_policy_ids;
		if(policy_ids===null || policy_ids.length===0 || typeof policy_ids === 'undefined'){
			console.log("Error302: the policy_ids arr is empty");
		} else {
			var full_msg = "";
			var full_err = false;
			policy_ids.map((policy_id, policy_index)=>{
				createTaskFromPolicy(Room, CurrentSockets[socket.id], {policy_id: policy_id}, function(err, msg, room_obj){
					console.log(err ? "Error307: "+msg : "Success307: "+msg+" "+JSON.stringify(room_obj));						
					full_msg.concat(msg);
					if(err) full_err = true;
					CurrentSockets[socket.id].room_obj = room_obj;
					io.to(socket.id).emit('ROOM', room_obj);
				});
			});
			if(full_err){
				io.to(socket.id).emit('msg', {err: full_err, msg: full_msg});	
			} else {
				io.to(socket.id).emit('msg', {err: false, msg: 'new tasks added'});	
			}
		}
	});
	socket.on('ACTIVE_TASK', function(task_obj){
		console.log('active task received: ', JSON.stringify(task_obj));
		storeActiveTask(Room, CurrentSockets[socket.id], 
			{task_id: task_obj.active_id, numerator_hours: task_obj.numerator_hours}, 
			function(err, msg, room_obj){
				console.log(err ? "Error340: "+msg+err : "room_obj at 340: "+JSON.stringify(room_obj));
				io.to(socket.id).emit('ROOM', room_obj);
				io.to(socket.id).emit('msg', {err: err, msg: msg});	
			}
		);
	});
});


function generateID(section_name='O'){
	var id = section_name.substr(0, 1)+"_"+Math.random().toString(36).substr(2, 9);
	return id;
}
function generateCurrentDateIndex(){
	var currentTime = new Date();
  var current_year = currentTime.getFullYear();
  var current_month = currentTime.getMonth();
  var current_day = currentTime.getDate();
  return (current_year+"-"+current_month+"-"+current_day);
}

function loginUser(data, auth, args, cb){
	var email = args.email;
	var password = args.password;
	var User = data;
	var bcrypt = auth.bcrypt;
	if(!(email&&password)) return cb(true, 'missing arguments');
	User.findOne({email: email}, function(err, user_obj){
		if(err) throw err;
		if(user_obj===null){
			return cb(true, 'credentials wrong');
		} else {
			bcrypt.compare(password, user_obj.password, function(err, boo) {
				if(err) throw err;
				if(boo===false){
					return cb(true, 'credentials wrong');
				} else {
					var msg = 'client logged in as '+user_obj.email;
					return cb(false, msg, user_obj);		
				}
			});
		}
	});
}


function addNamesToCurrentRooms(CurrentRooms, Rooms_arr, cb){
		
	Rooms_arr.map((room_obj)=>{
		if(!CurrentRooms[room_obj.id]){
			CurrentRooms[room_obj.id] = {id: room_obj.id, name: room_obj.name, users: {}};
		}
	});
	return cb(false, 'Names added to CurrentRooms');

}

function getCurrentRooms(CurrentRooms, room_ids, cb){
	var found_CurrentRooms = room_ids.map((room_id)=>{
			return CurrentRooms[room_id];
	});
	return cb(false, 'CurrentRooms found', found_CurrentRooms);
}

function checkAuth(UserRoom, user_id, room_id, cb){
	
	UserRoom.findOne({user_id: user_id, room_id: room_id}, function(err, UserRoom_obj){
		if(err) throw err;
		if(UserRoom_obj===null){
			return cb(true, 'no access to that room');
		} else {
			return cb(false, 'access granted');
		}
	});
}
function leaveRoom(CurrentRooms, room_id, target_email){
	var search_index = CurrentRooms[room_id].emails_arr.findIndex((email)=>{
		return (target_email === email)
	});
	if(search_index !==-1){
		CurrentRooms[room_id].emails_arr.splice(search_index, 1);
	}
}
function joinRoom(data, socket, args, cb){
	var CurrentRooms = data.CurrentRooms;
	var CurrentSockets = data.CurrentSockets;
	var room_obj = args.room_obj;
	var user_obj = args.user_obj;
	socket.join(room_obj.id);
	if(!CurrentRooms[room_obj.id]){
		CurrentRooms[room_obj.id] = {id: room_obj.id, name: room_obj.name, emails_arr: []};
	}
	
	CurrentSockets[socket.id].room_obj = room_obj;
	CurrentRooms[room_obj.id].emails_arr.push(user_obj.email);
	
	return cb(false, 'joined room', CurrentRooms)
}

function selectRoom(obj, socket, cb){
	switch (obj.active_room){
		default:
			return cb(false, 'room successfully selected');
	}
}
function createTaskFromPolicy(data, auth, args, cb){
	var room_obj = auth.room_obj;
	var policy_id = args.policy_id;
	
	
	var Room = data;
	Room.getByRoomID(room_obj._id, function(err, msg, found_room_obj){
		if(err){
			return cb(true, msg);
		} else {
			
			var policy_index = found_room_obj.policy_arr.findIndex((policy_obj)=>{
				return (policy_obj.id === policy_id)
			});
			if(policy_index ===-1){
				cb(true, 'policy not found');
			} else {
				var task_id = generateID('task');
				var new_task_obj = {
					id: task_id,
					policy_id: found_room_obj.policy_arr[policy_index].id, 
					name: found_room_obj.policy_arr[policy_index].name+'-'+task_id,
					numerator_hours: found_room_obj.policy_arr[policy_index].numerator_hours,
					denominator_hours: found_room_obj.policy_arr[policy_index].denominator_hours,
					status: 'ACTIVE'
				}
				//update db
				found_room_obj.task_arr.push(new_task_obj);
				Room.persistData(room_obj.id, found_room_obj, function(err, msg){
					console.log(err ? "Error472: "+msg+err : null);
					//update in_mem_data
					room_obj.task_arr.push(new_task_obj);
					return cb(false, 'task added to active tasks', room_obj);	
				});
			}
		}
	});
}
function storeActiveTask(data, auth, args, cb){
	var room_obj = auth.room_obj;
	var task_id = args.task_id;
	var numerator_hours = args.numerator_hours;
	var Room = data;
	
	//needs to be re-done because gantt_chart has different format now
	Room.getByRoomID(room_obj._id, function(err, msg, found_room_obj){
		if(err){
			return cb(true, msg);
		} else {
			var task_index = found_room_obj.task_arr.findIndex((task_obj)=>{
				return (task_obj.id === task_id)
			});
			if(task_index ===-1){
				cb(true, 'task not found');
			} else {
				//update task in db
				found_room_obj.task_arr[task_index].status = 'COMPLETE';
				found_room_obj.task_arr[task_index].numerator_hours = numerator_hours;

				//update gantt_obj in db
				var goal_name = found_room_obj.task_arr[task_index].name.split('-')[0];
				var currentTime = new Date();
    		var current_year = currentTime.getFullYear();
    		var current_month = currentTime.getMonth();
    		var current_day = currentTime.getDate();
    		var calendar_id = current_year+'-'+current_month+'-'+current_day;
    		var gantt_id = generateID('gantt');
				var new_gantt_obj = {
					id: gantt_id,
					goal_id: goal_name,
					calendar_id: calendar_id,
					week_id: current_year+'-wk'+getWeekNumber(currentTime),
					quarter_id: current_year+'-q'+Math.ceil(1+current_month/3),
					numerator_hours: found_room_obj.task_arr[task_index].numerator_hours,
					denominator_hours: found_room_obj.task_arr[task_index].denominator_hours
				};

				console.log('new gantt_obj: ', JSON.stringify(new_gantt_obj));
				var gantt_index = found_room_obj.gantt_arr.findIndex((gantt_obj)=>{
					return (gantt_obj.calendar_id === calendar_id && gantt_obj.goal_id === goal_name)
				});
				if(gantt_index===-1){
					console.log('i should be here');
					found_room_obj.gantt_arr.push(new_gantt_obj);
				} else {
					console.log('i should not be here')
					found_room_obj.gantt_arr[gantt_index].numerator_hours += new_gantt_obj.numerator_hours;
				}
				console.log('found_room obj: ', JSON.stringify(found_room_obj));
				Room.persistData(room_obj.id, found_room_obj, function(err, msg){
					console.log(err ? "Error505: "+msg+err : "Success505"+msg);
					//update in_mem data
					room_obj.task_arr[task_index].status = 'COMPLETE';
					room_obj.task_arr[task_index].numerator_hours = numerator_hours;
					if(gantt_index===-1){
						room_obj.gantt_arr.push(new_gantt_obj);
					} else {
						room_obj.gantt_arr[gantt_index].numerator_hours += numerator_hours;
					}
					return cb(false, 'active task successfully stored', room_obj);
				});
			}
		}
	});
}
	

function getWeekNumber(d) {
  // Copy date so don't modify original
  d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
  // Set to nearest Thursday: current date + 4 - current day number
  // Make Sunday's day number 7
  d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
  // Get first day of year
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
  // Calculate full weeks to nearest Thursday
  var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
  // Return array of year and week number
  return weekNo;
}

function getCookie(cookiename) 
  {
  // Get name followed by anything except a semicolon
  var cookiestring=RegExp(""+cookiename+"[^;]+").exec(document.cookie);
  // Return everything after the equal sign, or an empty string if the cookie name not found
  return decodeURIComponent(!!cookiestring ? cookiestring.toString().replace(/^[^=]+./,"") : "");
  }