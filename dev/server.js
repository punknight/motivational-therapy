var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io');
var serveStatic = require('serve-static');

app.get('/', function(req, res){
	res.sendFile(__dirname+'/index.html');
});

var public_path = __dirname.replace('dev', 'public');
app.use(serveStatic(public_path));

http.listen(process.env.PORT || 3000, function(){
	console.log('server running')
});


//I am testing the smallest component in my weekly chart app. 