var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var CurrentUsers = {};

app.get('/', function(req, res){
	res.write('<script src="/socket.io/socket.io.js"></script>');
	res.write('<script>var socket = io();</script>');
	res.end('<script>console.log(socket);</script>');
});

io.on('connection', function(socket){
	console.log('a new client connected');
	socket.on('GET POLICIES', function(){
		servePolicies([], CurrentUsers[socket.id], {}, function(err, msg, policy_arr){
			io.to(socket.id).emit('POLICIES', policy_arr);		
			io.to(socket.id).emit('msg', {err: err, msg: msg});	
		})
	})
})

http.listen(process.env.PORT || 3000, function(){
	console.log('server is up and running');
});

function generateID(section_name){
			var id = section_name.substr(0, 1)+"_"+Math.random().toString(36).substr(2, 9);
			return id;
		}
function servePolicies(data, auth, args, cb){
	var room_obj = auth.room_obj;
	return cb(false, 'policies found', room_obj.policy_arr);
}
function createPolicy(data, auth, args, cb){
	var room_id = auth.room_obj.id;
	var index = data.findIndex((room_obj)=>{
		return (room_obj.id === room_id)
	});
	if(index ===-1){
		cb(true, 'room not found');
	} else {
		data[index].policy_arr.push(args);
		auth.room_obj.policy_arr.push(args);
		cb(false, 'policy added');
	}
}
function createTaskFromPolicy(data, auth, args, cb){
	var room_id = auth.room_obj.id;
	var room_index = data.findIndex((room_obj)=>{
		return (room_obj.id === room_id)
	});
	if(room_index ===-1){
		cb(true, 'room not found');
	} else {
		var policy_index = data[room_index].policy_arr.findIndex((policy_obj)=>{
			return (policy_obj.id === args)
		});
		if(policy_index ===-1){
			cb(true, 'policy not found');
		} else {
			var task_id = generateID('task');
			var new_task_obj = {id: task_id, name: data[room_index].policy_arr[policy_index].name+'-'+task_id}
			data[room_index].task_arr.push(new_task_obj);
			auth.room_obj.task_arr.push(new_task_obj);
			cb(false, 'task added');
		}
	}
}
function serveTasks(data, auth, args, cb){
	var room_obj = auth.room_obj;
	return cb(false, 'tasks found', room_obj.task_arr);
}

if (require.main.filename == '/usr/local/lib/node_modules/mocha/bin/_mocha') {
	//Tests
	var assert = require('assert');
	
	describe('app', function(){
		var socket = {id: generateID('socket')};
		var room_id = generateID('room');
		var user_id = generateID('user');
		var policy_id = generateID('policy');
		var policy_arr = [{id: policy_id, name: 'Wag', description: 'Make $1000 using the Wag app by the end of the month.'}];
		var room_obj = {id: room_id, name: 'room1', policy_arr: policy_arr, task_arr: [], gantt_arr: []};
		var user_obj = {id: user_id, email: 'user1@mail.com', password: 'secret'}
		var room_data = [room_obj];
		CurrentUsers[socket.id] = {status: 'AUTH', user_obj: user_obj, room_obj: room_obj};
		describe('servePolicies', function(){
			it('should serve a policy_arr from DB in response to action "GET POLICIES".', function(done){
				servePolicies(room_data, CurrentUsers[socket.id], {}, function(err, msg, found_policy_arr){
					assert(found_policy_arr.length>0);
					assert(policy_arr==found_policy_arr);
					done();	
				});
			});
		});
		describe('createPolicy', function(){
			it('should create policy in DB in response to action "ADD POLICY".', function(done){
				var new_policy_id = generateID('policy');
				var new_policy_obj = {id: new_policy_id, name: 'TaskRabbit', description: 'Make $2000 using the TaskRabbit app by the end of the month.'};
				createPolicy(room_data, CurrentUsers[socket.id], new_policy_obj, function(err, msg){
					assert(new_policy_obj===room_data[0].policy_arr[1]);
					assert(new_policy_obj===CurrentUsers[socket.id].room_obj.policy_arr[1]);
					done();
				});
			});
		});
		describe('createTaskFromPolicy', function(){
			it('should create a task in DB from a policy already in DB in response to action "CREATE TASK".', function(done){
				createTaskFromPolicy(room_data, CurrentUsers[socket.id], policy_id, function(err, msg){
					console.log(msg);
					done();
				});
			});
		});
		describe('serveTasks', function(){
			it('should serve a task_arr from DB in response to action "GET TASKS".', function(done){
				serveTasks(room_data, CurrentUsers[socket.id], {}, function(err, msg){
					console.log(msg);
					done();
				});
			});
		});
	});
}