var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var serveStatic = require('serve-static');

app.get('/', function(req, res){
	res.sendFile(__dirname+"/index.html");
});

var public_path = __dirname.replace('dev', 'public');
app.use(serveStatic(public_path));

http.listen(process.env.PORT || 3000, function(){
	console.log('server is up and running');
});

io.on('connection', function(socket){
	console.log('a new client has connected');
});