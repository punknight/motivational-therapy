//add task data to gantt_arr
//remove task after it has been completed
//serve gantt_arr

var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var serveStatic = require('serve-static');

http.listen(process.env.PORT || 3000, function(){
	console.log('server is up and running');
});

app.get('/', function(req, res){
	res.sendFile(__dirname+'/index.html');
	console.log(__dirname);
});

app.use(serveStatic(__dirname.replace('dev','public')));

io.on('connection', function(socket){
	console.log('a new socket has connected');
});